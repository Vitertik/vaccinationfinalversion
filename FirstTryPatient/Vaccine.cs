﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTryPatient
{
    public class Vaccine
    {
        public int Id { get; set; }
        public string VaccineName { get; set; }

        public DateTime VaccineDate { get; set; }

        public string Status { get; set; }


        public int PatientId { get; set; }
        public Patient Patient { get; set; }
    }
}
