﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FirstTryPatient
{
    /// This program is suppoused to used by medical staff to register, make vaccination against COVID 19 and print passport 

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();     
        }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {
           if ( txtFirstname.Text == "")
            {
                MessageBox.Show("You need to type a first name", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtLastname.Text == "")
            {
                MessageBox.Show("You need to type a last name", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtSinNum.Text == "")
            {
                MessageBox.Show("You need to type a SIN number", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtEmail.Text == "")
            {
                MessageBox.Show("You need to type an email address", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try {

                int tempSinNum = int.Parse(txtSinNum.Text);
                Patient fetchedPatient = (from p in Global.context.patients
                                          where p.SinNumber == tempSinNum
                                          select p).FirstOrDefault<Patient>();

                if (fetchedPatient != null) {
                    MessageBox.Show("Patient with this SIN number has been already registered", "Message",
                                               MessageBoxButton.OK, MessageBoxImage.Exclamation); return; }
            }
            catch ( SystemException  ex)
            { MessageBox.Show(ex.Message, "Some error", MessageBoxButton.OK, MessageBoxImage.Warning); return; }
            

            try {
                Patient tempPatient = new Patient()
                {
                    FirstName = UppercaseFirstLetter(txtFirstname.Text),
                    LastName = UppercaseFirstLetter(txtLastname.Text),
                    SinNumber = int.Parse(txtSinNum.Text),
                    Email = txtEmail.Text
                };
                Global.context.patients.Add(tempPatient);
                Global.context.SaveChanges();

                if(MessageBoxResult.Yes != MessageBox.Show("You succesfully registered a patient \n Do you want to open the file?", "Question",
                    MessageBoxButton.YesNo, MessageBoxImage.Question))
                { clearFields(); return; }

                UserRoom userRoom = new UserRoom(tempPatient);
                userRoom.Show();
                // userRoom.Owner = this;
                Close();
            }
            catch(SystemException ex)         
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void clearFields()
        {
            txtFirstname.Text = "";
            txtLastname.Text = "";
            txtSinNum.Text = "";
            txtEmail.Text = "";
        }

        private void btnSignIn_Click(object sender, RoutedEventArgs e)
        {
            if (txtFirstname.Text == "")
            {
                MessageBox.Show("You need to type a first name", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtLastname.Text == "")
            {
                MessageBox.Show("You need to type a last name", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtSinNum.Text == "")
            {
                MessageBox.Show("You need to type a SIN number", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtEmail.Text == "")
            {
                MessageBox.Show("You need to type an email address", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string tempFirstName = txtFirstname.Text;
            string tempLastName = txtLastname.Text;
            int tempSinNumber = int.Parse(txtSinNum.Text);
            string tempEmail = txtEmail.Text;

            Patient fetchedPatient = (from p in Global.context.patients where p.Email == tempEmail
                                       &&  p.SinNumber == tempSinNumber
                                       && p.FirstName == tempFirstName
                                       && p.LastName == tempLastName
                                      select p).FirstOrDefault<Patient>();
             

            if (fetchedPatient != null)
            {
                MessageBox.Show("You succesfully signed in!");

                UserRoom userRoom = new UserRoom(fetchedPatient);
                userRoom.Show();
                
                Close();
            }
            else {
                MessageBox.Show("Make sure the details you've entered are correct", "Information",
                    MessageBoxButton.OK, MessageBoxImage.Information); 
            }           
        }

        static string UppercaseFirstLetter(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            
            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}
