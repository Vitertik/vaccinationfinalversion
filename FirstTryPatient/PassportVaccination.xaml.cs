﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FirstTryPatient
{
    public partial class PassportVaccination : Window
    {
        public DateTime Date { get; set; }
        public PassportVaccination(Patient patient)
        {
            InitializeComponent();

            txtPatientName.Text = "Patient Name:  " + patient.FirstName + " " + patient.LastName;
            txtPassNo.Text = patient.PatientId.ToString();
            Date = DateTime.Now;
            txtDataIssued.Text = Date.ToString();

            Vaccine vaccine1 = (from v in Global.context.vaccines where v.PatientId == patient.PatientId select v).FirstOrDefault<Vaccine>();
            

            Vaccine vaccine2 = (from v in Global.context.vaccines
                                where v.PatientId == patient.PatientId
                                orderby v.Id descending
                                select v).FirstOrDefault<Vaccine>();

            txtFirstvacDate.Text = vaccine1.VaccineDate.ToString();
            txtSecondvacDate.Text = vaccine2.VaccineDate.ToString();

            txtDueDate.Text = Date.AddDays(30).ToString();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                this.IsEnabled = false;
                PrintDialog printDialog = new PrintDialog();
                if (printDialog.ShowDialog() == true)
                {
                    printDialog.PrintVisual(print, "PassportVaccination");
                }
            }
            finally
            {
                this.IsEnabled = true;
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
