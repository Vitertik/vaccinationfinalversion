﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FirstTryPatient
{
    public partial class UserRoom : Window
    {
        Patient currPatient;
        Vaccine currentVac1;
        Vaccine currentVac2;

        public UserRoom(Patient patient)
        {
            InitializeComponent();

            currPatient = patient;

            try
            {
                txtFirstName.Text = patient.FirstName;
                txtLastName.Text = patient.LastName;
                txtSinNum.Text = patient.SinNumber.ToString();
                txtEmail.Text = patient.Email;

                dpFirstVacDate.DisplayDateStart = DateTime.Today;

                Vaccine vaccine1 = (from v in Global.context.vaccines where v.PatientId == patient.PatientId select v).FirstOrDefault<Vaccine>();
                currentVac1 = vaccine1;

                Vaccine vaccine2 = (from v in Global.context.vaccines
                                    where v.PatientId == patient.PatientId
                                    orderby v.Id descending
                                    select v).FirstOrDefault<Vaccine>();
                currentVac2 = vaccine2;

                if (vaccine1 != null)
                {
                    cbVacName.Text = vaccine1.VaccineName;
                    dpFirstVacDate.SelectedDate = vaccine1.VaccineDate;
                    cbFirstDateStatus.Text = vaccine1.Status;
                }

                if (vaccine2 != null)
                {
                    dpSecondVacDate.SelectedDate = vaccine2.VaccineDate;
                    cbSecondDateStatus.Text = vaccine2.Status;
                }
            }
            catch (SystemException ex)        
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.IsEnabled = true;
            txtLastName.IsEnabled = true;
            txtEmail.IsEnabled = true;
        }
        private void btnsaveChanges_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Patient patientToUpdate = currPatient;
                patientToUpdate.FirstName = txtFirstName.Text;
                patientToUpdate.LastName = txtLastName.Text;
                patientToUpdate.Email = txtEmail.Text;

                Global.context.SaveChanges();
            }
            catch (SystemException ex)         
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            txtFirstName.IsEnabled = false;
            txtLastName.IsEnabled = false;
            txtEmail.IsEnabled = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (currentVac1 != null && currentVac2 != null)
            {
                try
                {
                    currentVac1.VaccineName = cbVacName.Text;  
                    currentVac1.VaccineDate = dpFirstVacDate.SelectedDate.Value;
                    currentVac1.Status = cbFirstDateStatus.Text;

                    currentVac2.VaccineName = cbVacName.Text;
                    currentVac2.VaccineDate = dpSecondVacDate.SelectedDate.Value;
                    currentVac2.Status = cbSecondDateStatus.Text;

                    Global.context.SaveChanges();
                    MessageBox.Show("Changes saved");
                }

                catch (SystemException ex)         
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }

            else 
            {
                try
                {
                    currentVac1 = new Vaccine { VaccineName = cbVacName.Text, VaccineDate = dpFirstVacDate.SelectedDate.Value, Status = cbFirstDateStatus.Text, PatientId = currPatient.PatientId };
                    currentVac2 = new Vaccine { VaccineName = cbVacName.Text, VaccineDate = dpSecondVacDate.SelectedDate.Value, Status = cbSecondDateStatus.Text, PatientId = currPatient.PatientId };
                    Global.context.vaccines.Add(currentVac1);
                    Global.context.vaccines.Add(currentVac2);
                    Global.context.SaveChanges();
                    MessageBox.Show("Changes saved");
                }
                catch (SystemException ex)         
                {
                    MessageBox.Show("Error: " + ex.Message);
                    return;
                }
            }
            cbVacName.IsEnabled = false;
            dpFirstVacDate.IsEnabled = false;
            cbFirstDateStatus.IsEnabled = false;
            dpSecondVacDate.IsEnabled = false;
            cbSecondDateStatus.IsEnabled = false;
        }

        private void btnPassRequest_Click(object sender, RoutedEventArgs e)
        {
            if(currentVac1 == null) { return; }

            if (currentVac1.Status == "Done" && currentVac2.Status == "Done")
            {
                PassportVaccination passport = new PassportVaccination(currPatient);
                passport.Show();
            }
            else
            {
                MessageBox.Show("You can print a passport only after first\n and second vaccinations are done", "Warning",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
        }
            
        private void btnModifyVaccine_Click(object sender, RoutedEventArgs e)
        {
            cbVacName.IsEnabled = true;
            dpFirstVacDate.IsEnabled = true;
            cbFirstDateStatus.IsEnabled = true;
            dpSecondVacDate.IsEnabled = true;
            cbSecondDateStatus.IsEnabled = true;
        }
    }
}
