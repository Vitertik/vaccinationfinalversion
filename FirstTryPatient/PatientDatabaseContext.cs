﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace FirstTryPatient
{
    class PatientDatabaseContext : DbContext
    {
        const string databasename = "VaccinationDatabase5.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, databasename);


        
        public PatientDatabaseContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30")
        {
        }

        public DbSet<Patient> patients { get; set; }

        public DbSet<Vaccine> vaccines { get; set; }
    }
}
