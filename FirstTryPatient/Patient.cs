﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace FirstTryPatient
{
     public class Patient
    {
        private int _PatientId;
        public int PatientId
        {
            get
            {
                return _PatientId;
            }
            set
            {
                _PatientId = value;
            }
        }

        [Required]
        [StringLength(30)]
        private string _FirstName;
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                string pattern = @"^[A-Za-z]{2,30}$";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(value))
                {
                    throw new ArgumentException("First Name must be between 2 and 30 characters and no letters allowed");
                }
                _FirstName = value;
            }
        }
        [Required]
        [StringLength(30)]
        private string _LastName;
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                string pattern = @"^[A-Za-z]{2,30}$";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(value))
                {
                    throw new ArgumentException("Last Name must be between 2 and 30 characters and no letters allowed");
                }
                _LastName = value;
            }
        }

        [Required]
        
        private int _SinNumber;
        public int SinNumber
        {
            get
            {
                return _SinNumber;
            }
            set
            {
                string pattern = @"^[0-9]{3}$";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(value.ToString()))
                {
                    throw new ArgumentException("Sin Number must contain 3 numbers");
                }
                _SinNumber = value;
            }
        }

        [Required]

        private string _Email;
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                string pattern = @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(value))
                {
                    throw new ArgumentException("Check your email. Email must look like NAME@GMAIL.COM ");
                }
                _Email = value;
            }
        }
        public ICollection<Vaccine> Vaccine { get; set; }

    }
}
